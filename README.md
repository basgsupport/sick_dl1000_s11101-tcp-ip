---

## About

This is a sample code for interfacing with [SICK_DL1000_S11101](https://www.sick.com/sg/en/distance-sensors/long-range-distance-sensors/dx1000/dl1000-s11101/p/p455158) distance sensor. 


---

## How to use

The interface use TwinCAT TCP/IP functions.
Please download and install [TF6310 TC3 TCP/IP](https://www.beckhoff.com/english/download/tc3-download-tf6xxx.htm) from Beckhoff website.



---

## Details

TwinCAT is a TCP client and SICK sensor is the TCP server. We suggest using serial terminal (e.g. Hyperterminal / Hercules) first to check the TCP/IP connection to the SICK sensor.

TwinCAT -> SICK:
`<STX>sRN Distance<ETX>`

SICK -> TwinCAT:
`<STX>sRA Distance D52<ETX>`

D52(hex) means 3410(dec).

[SICK sensor telegram documentation](https://cdn.sick.com/media/docs/9/39/339/Telegram_listing_Telegram_Listing_Dx1000_en_IM0075339.PDF)


There is a function inside the code that will parse this data. Please look at the comments in the code.


---

## Help

Please contact `support@beckhoff.com.sg`